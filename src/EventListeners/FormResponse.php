<?php

namespace Drupal\spa\EventListeners;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class FormResponse implements EventSubscriberInterface {

  public function loginRedirect(ResponseEvent $event) {
    $request = $event->getRequest();
    $response = $event->getResponse();

    if ($request->attributes->get('_route') === 'user.login.http') {
      dump($event);
      die('here to dAY');
    }

  }
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => ['loginRedirect', -100],
    ];
  }

}
