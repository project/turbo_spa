import * as Turbo from "@hotwired/turbo"
//import './utils/prefetch'

// Expose Turbo to the rest of the app to allow for dynamic Turbo calls
window.Turbo = Turbo


// want your code to be executed on every page load, like analytics code

document.addEventListener('turbo:load', () => {

})
