const path = require('path')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

let mode = "development",
  target = "web",
  source_map = "source-map"

if (process.env.NODE_ENV === "production") {
  mode = "production"
  target = "browserslist"
  source_map = "eval"
}
module.exports = {
  mode: mode,

  entry: {
    'turbo_entry': './src/turbo_entry.js',
    'main': './src/main.js',
  },
  output: {
    assetModuleFilename: "images/[hash][ext][query]",
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, '../dist'),
  },

  plugins: [
    new MiniCssExtractPlugin(),
    new CleanWebpackPlugin()
  ],

  module: {
    rules: [
      {
        test: /\.(s[ac]|c)ss$/i,
        use: [
          //'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          // Compiles Sass to CSS
          'sass-loader'
        ]
      },

      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ],
  },

  devtool: source_map,
}
